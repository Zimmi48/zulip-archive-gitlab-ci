#!/bin/bash

# The toplevel script to rebuild a Zulip archive manually or in an automated CI process.
# Takes care of installing project dependencies (unless a 'deps-done' file exists),
# and of generating a zuliprc file (if missing) from environment variables.

cat <<EOF
This script requires the following variables to be defined in the environment:

  - ARCHIVE_SITE_URL: the URL from which the HTML archives will be served,
    for example:
      ARCHIVE_SITE_URL="file:///$(pwd)/public"  # local testing
      ARCHIVE_SITE_URL="$CI_PAGES_URL"          # gitlab CI
  - ARCHIVE_HTML_ROOT: the subdirectory of ARCHIVE_SITE_URL containing the archives
    for example, with
      ARCHIVE_HTML_ROOT=archive
    the archives will be at $ARCHIVE_SITE_URL/$ARCHIVE_HTML_ROOT
  - ARCHIVE_ICON_URL: the URL of a SVG file with a square icon that will be shown
    before each message, for example:
      ARCHIVE_ICON_URL="https://upload.wikimedia.org/wikipedia/commons/3/39/Zulip-icon-square.svg"
    Alternatively, you can store an icon.svg file at the root of the repository.

In addition, unless a './zuliprc' file is present in the current
directory, the following variables are required:

  - ZULIPRC_API_EMAIL: the "bot email" of your Zulip archive bot
    looks like: archive-bot@foobar.zulipchat.com
  - ZULIPRC_API_KEY_SECRET: the "API key" of your Zulip archive bot
    looks like: Z6CMQ5FBXFwdzEuPe9pBJ8jeGoVRJEjo
  - ZULIPRC_API_SITE: the URL of your Zulip chat
    looks like: https://typ.zulipchat.com

Note: the ZULIPRC_* data corresponds precisely to the metadata file Zulip gives
for bots. If you have already created a bot, you can get the data from
  Settings > Your bots > Download/Paste zuliprc (icons)

If you are running this script from a shell, you should include the 'zuliprc' file
in the working directory, or export the ZULIPRC_API_* variables manually.
Be sure not to store the ZULIPRC_API_KEY_SECRET in public files, it is a secret.

If you are running this script in a Gitlab CI script, you should
define the ARCHIVE_* variables in the "variables" block of your script,
and define the ZULIPRC_* variables manually in the pipeline
configuration interface, to avoid making them publicly available.


EOF


missing_variable() {
  echo "Error: the variable $1 is missing. Abort"
  exit 1
}
if [ ! -v ARCHIVE_SITE_URL ]; then missing_variable "ARCHIVE_SITE_URL"; fi
if [ ! -v ARCHIVE_HTML_ROOT ]; then missing_variable "ARCHIVE_HTML_ROOT"; fi
if [ ! -f icon.svg -a ! -v ARCHIVE_ICON_URL ]; then missing_variable "ARCHIVE_ICON_URL"; fi

if [ ! -f ./zuliprc ]
then
    if [ ! -v ZULIPRC_API_EMAIL ]; then missing_variable "ZULIPRC_API_EMAIL"; fi
    if [ ! -v ZULIPRC_API_KEY_SECRET ]; then missing_variable "ZULIPRC_API_KEY_SECRET"; fi
    if [ ! -v ZULIPRC_API_SITE ]; then missing_variable "ZULIPRC_API_SITE"; fi
fi
# Provide the environment variables expected by settings.py
export PROD_ARCHIVE=1
export SITE_URL=$ARCHIVE_SITE_URL
export HTML_ROOT=$ARCHIVE_HTML_ROOT
export ZULIP_ICON_URL=$SITE_URL/icon.svg

## Note: JSON_DIRECTORY is the part that needs to be cached
## to avoid re-downloading all the data each time
export JSON_DIRECTORY="$(pwd)/cache/zulip_json"

export HTML_DIRECTORY="$(pwd)/zulip-archive/archive"

if [ -f zuliprc ]
then
    echo "found a ./zuliprc file, it will be used as-is"
else
    echo "generating a ./zuliprc from environment variables"
    set +x
    rm -f zuliprc
    echo "[api]" >> zuliprc; \
    echo "email=$ZULIPRC_API_EMAIL" >> zuliprc; \
    echo "key=$ZULIPRC_API_KEY_SECRET" >> zuliprc; \
    echo "site=$ZULIPRC_API_SITE" >> zuliprc
fi

echo
echo "# Create caching and publication directories"

echo "- cache/: stores the fetched chat data"
echo "- public/: stores the web output"
mkdir -p cache public

echo
echo "# Install Zulip and Jekyll (unless a file deps-done exists)"
if [ -f deps-done ]
then
    echo "Found a file deps-done, not installing dependencies."
else
    echo "Installing Zulip+Jekyll dependencies."

    # Zulip dependencies described in
    #   https://github.com/zulip/zulip-archive/blob/master/instructions.md
    sudo apt-get install python3 python3-pip -y
    pip3 install zulip pyyaml

    sudo apt-get install jekyll -y
fi

echo
echo "# Install the zulip-archive script"
if [ -d zulip-archive ]
then
    echo "A zulip-archive/ directory exists, we assume it is a clone of"
    echo "  https://github.com/zulip/zulip-archive"
else
    echo "We clone the zulip-archive source to ./zulip-archive/ and (non-)configure it."
    git clone https://github.com/zulip/zulip-archive zulip-archive || exit 2
    (cd zulip-archive; cp default_settings.py settings.py)
    (cd zulip-archive; cp default_streams.yaml streams.yaml)
fi

echo
echo "# Build the HTML files"
# build the HTML archive using secret file ./zuliprc,
# storing JSON data in cache/zulip_json,
# and writing the final results to ./public"
bash rebuild-archive.sh zuliprc ./public

echo
echo "# Copy the icon"
if [ ! -f icon.svg ]
then
    echo "iconv.svg not found, fetching it from $ARCHIVE_ICON_URL"
    wget $ARCHIVE_ICON_URL -O icon.svg
fi
cp icon.svg public/

echo "# Build done."
echo "xdg-open public/$HTML_ROOT/index.html"
