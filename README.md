This repository contains scripts to build a HTML archive of a Zulip
chat, using the export program
[zulip-archive](https://github.com/zulip/zulip-archive/).

Here arae two supported ways to use those scripts:
- Manually, from your machine; we recommend doing this as first for
  testing.
- Automatically in a Gitlab CI pipeline; we recommend setting
  a scheduled pipeline to refresh the archive every day.

This "Gitlab CI pipeline" approach is an alternative to the "Github
Actions" setup proposed by
[zulip-archive](https://github.com/zulip/zulip-archive/). One reason
to avoid "Github Actions" is that the automation setup currently
requires permission to access and update all repositories of the
github account hosting the archive, which is a liability.
 
## Known deployments

- [archives](https://gasche.gitlab.io/types-chat-archive/) of the [Types zulip chat](typ.zulipchat.com).
  https://gitlab.com/gasche/types-chat-archive/

## In this repository

- [archive-script.sh](archive-script.sh): the main script, which will take care of installing dependencies,
  cloning the zulip-archive repository, and running the archival process. 
  This script is directed by environment variables.
- [rebuild-archive.sh](rebuild-archive.sh): an auxiliary script that only runs the archival process;
  may be used directly if you have already done the configuration manually.
- [zulip-archive-gitlab-pages.yml](zulip-archive-gitlab-pages.yml): a YAML file directing the Gitlab CI to use
  the script (our [.gitlab-ci.yml](.gitlab-ci.yml) just includes it)


## How to manually run the script on your machine

1. Clone the repository and move to its directory.

2. If you are not using a Debian-family machine, you need to setup the
   dependencies by hand (zulip, pyyaml, jekyll, git). Then create
   a file `deps-done` in the repository directory to ask the script to
   skip this step.

   Example (for Fedora):

   ```sh
   sudo dnf install python3 python3-pip rubygems git
   pip install zulip pyyaml
   gem install jekyll
   touch deps-done
   ```

3. Decide which icon/logo you want to see shown on each message. This could be the Zulip logo,
   or the logo/icon of your own organization. You can specify this by URL, for example
     https://upload.wikimedia.org/wikipedia/commons/3/39/Zulip-icon-square.svg
   or by importing a file 'icon.svg' in the repository directory.

4. On your Zulip chat, create a  bot from your account:
     Settings > Your bots > Add a new bot, "Generic" bot type

   Download the 'zuliprc' file for this bot (Active bots > click the download icon on the bot),
   and copy it in the repository directory. 

   Note: This file contains information needed to authenticate to
   Zulip and fetch your chat content.  In particular it contains the
   "zulip API key" of this bot, which is a secret (having it let
   people consume Zulip API resources on your behalf), so don't share
   it publicly.

5. If you wish to use a specific version of the zulip-archive script,
   you can clone and configure it inside the repository directory.

   Otherwise the next step will clone and configure it automatically
   (on the first run).

6. At this point you should have `deps-done`, `zuliprc/` and optionally
   `icon.svg` and `zulip-archive/` in the repository directory.

   You can now run the script:

   ```sh
   ARCHIVE_SITE_URL="file:///$(pwd)/public" \
   ARCHIVE_HTML_ROOT="" \
   ARCHIVE_ICON_URL="https://upload.wikimedia.org/wikipedia/commons/3/39/Zulip-icon-square.svg" \
   bash archive-script.sh
   ```

   (The ARCHIVE_ICON_URL setting is unnecesasry if you have an icon.svg file)

   This script will download the Zulip data in JSON format in
   a `cache/` directory, and generate the corresponding HTML output in
   a `public/` directory. You can now browse the archive:

   ```sh
   xdg-open public
   ```
   
   Keeping the `cache/` around will make the script run faster next
   time, by only downloading new content.

   ARCHIVE_HTML_ROOT can be used to specify a subdirectory in which to
   include the HTML files; for example, ARCHIVE_HTML_ROOT=archive will
   put the html files in public/archive (but some assets remain in
   public/), so that it is in a subdirectory of your final website.


## How to run the script as a Gitlab CI pipeline

1. Fork the repository as your own Gitlab project.
   (This works on gitlab.com, but should also work on other Gitlab instances if they enable CI and Pages.)

2. Make sure that CI/CD and Pages are enabled for your repository.
   (Settings > General > Visibility, project features, permissions)

3. On your Zulip chat, create a bot from your account:
     Settings > Your bots > Add a new bot, "Generic" bot type

   Download the 'zuliprc' file for this bot (Active bots > click the download icon on the bot),
   and copy it to your machine so you can see the content.
   (Do *not* commit it in the repository.)

   Note: This file contains information needed to authenticate to
   Zulip and fetch your chat content.  In particular it contains the
   "zulip API key" of this bot, which is a secret (having it let
   people consume Zulip API resources on your behalf), so don't share
   it publicly.

4. On your Gitlab repo, set the variables expected by the script for Zulip API access:
     Settings > CI/CD > Variables

   You need to set three variables, corresponding to the three fields of zuliprc:
    - ZULIPRC_API_EMAIL: the "bot email" of your Zulip archive bot
      looks like: archive-bot@foobar.zulipchat.com
    - ZULIPRC_API_SITE: the URL of your Zulip chat
      looks like: https://typ.zulipchat.com
    - ZULIPRC_API_KEY_SECRET: the "API key" of your Zulip archive bot
      looks like: Z6CMQ5FBXFwdzEuPe9pBJ8jeGoVRJEjo

   The ZULIPRC_API_KEY_SECRET should be "Masked" (this is a setting),
   so that it does not appear in the CI logs.

5. Run a manual pipeline for testing:
     CI/CD > Pipelines > Run pipeline
   (no need to specify extra variables)

   Look at the logs if it fails, tweak the config and retry.

   On a succesful run, the CI logs should give you the URL of the HTML archives.

   At this point, the archive is publicly available at the URL,
   corresponding to the chat content at the time you ran the manual
   pipeline.

   Any future pipeline run (manual or scheduled) will update the website.

6. Setup a "Scheduled pipeline" to refresh the archive daily.
     CI/CD > Schedules > New Schedule

The chat archive will be updated automatically by the following events:
- a push or MR merge in this repository
- a manual pipeline run
- each execution of the scheduled pipeline

In our tests, building the archive typically takes 2mn. The first run
may be longer as it needs to download the full available history of
your Zulip chat.


Note: the ARCHIVE_* configuration variables are set in
[zulip-archive-gitlab-pages.yml](zulip-archive-gitlab-pages.yml). Their default setting should be
fine for most usage, but you may still want to modify them.


## Customizing the output

If a `zulip-archive` directory exists at the root of this repository,
the script will assume that it contains a compatible and
correctly-configured version of the zulip-archive script and use it.

If an 'icon.svg' file exists at the root of this repository, it will be
used as the chat icon/logo in the HTML output.

If a `layout/` directory exists at the root of this repository, it
will be passed to Jekyll to customize the HTML output. By default we
use the zulip-archive default layout and style file, which basically
do not provide any styling.

If a `style.css` file exists at the root of this repository, all
`*.css` files at the root will be copied in the final site.


## If something goes wrong


#### Probably the dependencies

If someday your web archive become broken or stops updating, the cause
will probably be that one of the dependencies of this script stopped
working. It is supposed to be written defensively, so that failures
abort the run and do not update the archive, but this may not work as
expected.

In particular, currently we just clone the current master branch of
[zulip-archive](https://github.com/zulip/zulip-archive/), so we could
easily break if the script changes its interface. (But we also
automatically benefit from their rendering improvements!)


#### Open an issue!

Please remember to open an issue to let us known about the issue, so
that we can try to fix the script.


#### Get the data

The JSON and HTML files of the Zulip archive are stored as 'artifacts'
on each run of the Gitlab pipeline, so you should be able to recover
them by going to "CI/CD > Pipelines", and then clicking on the
three-vertical-dots icon at the right of each pipeline run.


#### Untested recovery plan

The best recommendation if the present script stops working due to
a zulip-archive incompatibility (but we have not encountered this
scenario yet) is to:

- clone it on your local machine

- in its working directory, clone a known-to-work version of zulip-archive
  (for example
   [gasche/zulip-archive#known-to-work-with-gasche-archive-script](https://github.com/gasche/zulip-archive/releases/tag/known-to-work-with-gasche-archive-script))

- rerun the script manually, checking that it works

- commit the working zulip-archive/ sources within the repository
  (yep, ugly hack) and push this.

This should rerun your pipeline with a known-good version of
zulip-archive, and hopefully restore the HTML content as you expect
(possibly to an older-looking version of the output).


